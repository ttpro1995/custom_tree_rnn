from __future__ import print_function
import data_utils
import tree_rnn
import tree_lstm
import tree_gru
import numpy as np
import theano
from theano import tensor as T
import random
import pickle
import os

import log_util

logger = log_util.create_logger("sentiment",False)
logger.info('-------------------------START--------------------------')


DIR = './data/sst'  # '../treelstm/data/sst'
GLOVE_DIR = './data' # '../treelstm/data'  # should include .npy files of glove vecs and words
FINE_GRAINED = False
DEPENDENCY = True
SEED = 88

NUM_EPOCHS = 50
LEARNING_RATE =	0.05

EMB_DIM = 300
HIDDEN_DIM = 168 # default 100


logger.info('FINE_GRAINED %s'%(FINE_GRAINED))
logger.info('DEPENDENCY %s'%(DEPENDENCY))
logger.info('SEED %d'%(SEED))
logger.info('NUM_EPOCHS %d'%(NUM_EPOCHS))
logger.info('LEARNING_RATE %f'%(LEARNING_RATE))
logger.info('EMB_DIM %d'%(EMB_DIM))
logger.info('HIDDEN_DIM %d'%(HIDDEN_DIM))
logger.info('Tree model NaryTreeLSTM')

class SentimentModel(tree_lstm.ChildSumTreeLSTM): # default ChildSumTree
    def train_step_inner(self, x, tree, y, y_exists):
        self._check_input(x, tree)
        return self._train(x, tree[:, :-1], y, y_exists)

    def train_step(self, root_node, label):
        x, tree, labels, labels_exist = \
            tree_rnn.gen_nn_inputs(root_node, max_degree=self.degree,
                                   only_leaves_have_vals=False,
                                   with_labels=True)
        y = np.zeros((len(labels), self.output_dim), dtype=theano.config.floatX)
        y[np.arange(len(labels)), labels.astype('int32')] = 1
        loss, pred_y = self.train_step_inner(x, tree, y, labels_exist)
        return loss, pred_y

    def loss_fn_multi(self, y, pred_y, y_exists):
        return T.sum(T.nnet.categorical_crossentropy(pred_y, y) * y_exists)


def get_model(num_emb, output_dim, max_degree):
    return SentimentModel(
        num_emb, EMB_DIM, HIDDEN_DIM, output_dim,
        degree=max_degree, learning_rate=LEARNING_RATE,
        trainable_embeddings=True,
        labels_on_nonroot_nodes=True,
        irregular_tree=DEPENDENCY)

def train():
    vocab, data = data_utils.read_sentiment_dataset(DIR, FINE_GRAINED, DEPENDENCY)

    train_set, dev_set, test_set = data['train'], data['dev'], data['test']
    max_degree = data['max_degree']
    print('train %d' %(len(train_set)))
    print('dev %d'% (len(dev_set)))
    print('test %d'% (len(test_set)))
    print('max degree %d' %(max_degree))


    logger.info('train %d' %(len(train_set)))
    logger.info('dev %d'% (len(dev_set)))
    logger.info('test %d'% (len(test_set)))
    logger.info('max degree %d' %(max_degree))


    num_emb = vocab.size()
    num_labels = 5 if FINE_GRAINED else 3
    for key, dataset in list(data.items()):
        if key == 'max_degree':
            continue
        labels = [label for _, label in dataset]
        assert set(labels) <= set(range(num_labels)), set(labels)
    print('num emb %d' %(num_emb))
    print('num labels %d' %(num_labels))
    logger.info('num emb %d' %(num_emb))
    logger.info('num labels %d' %(num_labels))

    random.seed(SEED)
    np.random.seed(SEED)
    model = get_model(num_emb, num_labels, max_degree)

    # initialize model embeddings to glove
    embeddings = model.embeddings.get_value()
    glove_vecs = np.load(os.path.join(GLOVE_DIR, 'glove.npy'))
    glove_words = np.load(os.path.join(GLOVE_DIR, 'words.npy'))
    glove_word2idx = dict((word, i) for i, word in enumerate(glove_words))
    for i, word in enumerate(vocab.words):
        if word in glove_word2idx:
            embeddings[i] = glove_vecs[glove_word2idx[word]]
    glove_vecs, glove_words, glove_word2idx = [], [], []
    model.embeddings.set_value(embeddings)
    last_def_score = 1
    for epoch in range(NUM_EPOCHS):
        print('epoch %d'%(epoch))
        logger.info('epoch %d' % (epoch))
        avg_loss = train_dataset(model, train_set)
        print('avg loss %f' %(avg_loss))
        logger.info('avg loss %f' % (avg_loss))
        dev_score = evaluate_dataset(model, dev_set)
        data_utils.save_model(model, epoch,dev_score)
        diff_score = abs(dev_score - last_def_score)
        if (diff_score < 0.001):
            logger.info('Early stop')
            break # converge

        print('dev score %f' %(dev_score))
        logger.info('dev score %f' % (dev_score))



    print('finished training')
    test_score = evaluate_dataset(model, test_set)
    print('test score %f' %(test_score))
    logger.info('test score %f' % (test_score))


def train_dataset(model, data):
    losses = []
    avg_loss = 0.0
    total_data = len(data)
    for i, (tree, _) in enumerate(data):
        loss, pred_y = model.train_step(tree, None)  # labels will be determined by model
        losses.append(loss)
        avg_loss = avg_loss * (len(losses) - 1) / len(losses) + loss / len(losses)
        if (i% 1000 == 0):
            print('avg loss %.2f  at example %d of %d\r' % (avg_loss, i, total_data), end=' ')
    return np.mean(losses)


def evaluate_dataset(model, data):
    num_correct = 0
    for tree, label in data:
        pred_y = model.predict(tree)[-1]  # root pred is final row
        num_correct += (label == np.argmax(pred_y))

    return float(num_correct) / len(data)


if __name__ == '__main__':
    train()
    logger.info('-----------------DONE-----------------------------')
