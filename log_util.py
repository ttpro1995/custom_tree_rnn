import logging

def create_logger(logger_name, print_console = False):
    """
    Create a logger write to file logger_name.log
    :param logger_name: name of the file
    :param print_console: True = print log on console (also write to file). Default False
    :return: logger
    """
    FORMAT = '%(asctime)s : %(levelname)s : %(message)s'
    logFormatter = logging.Formatter(FORMAT)
    logging.basicConfig(filename=logger_name + '.log', level=logging.DEBUG, format=FORMAT)
    # logging.basicConfig(level=logging.DEBUG, format=FORMAT)
    logger = logging.getLogger(logger_name)
    if (print_console):
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(logFormatter)
        logger.addHandler(console_handler)
    return logger
